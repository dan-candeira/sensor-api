from rest_framework import viewsets, mixins
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from . import models, serializers

'''
    Equipment related views
'''


class EquipmentViewSet(viewsets.GenericViewSet,
                       mixins.ListModelMixin,
                       mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Equipment.objects.all()
    serializer_class = serializers.EquipmentSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


class SensorViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Sensor.objects.all()
    serializer_class = serializers.SensorSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


class SensorTypeViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.SensorType.objects.all()
    serializer_class = serializers.SensorTypeSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


'''
    Patient related views
'''


class PatientViewSet(viewsets.GenericViewSet,
                     mixins.ListModelMixin,
                     mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Patient.objects.all()
    serializer_class = serializers.PatientSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


class CollectViewSet(viewsets.GenericViewSet,
                     mixins.ListModelMixin,
                     mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Collect.objects.all()
    serializer_class = serializers.CollectSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


class SampleViewSet(viewsets.GenericViewSet,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.Sample.objects.all()
    serializer_class = serializers.SampleSerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()


'''
    Patient and equipment related views
'''


class UseHistoryViewSet(viewsets.GenericViewSet,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin):
    """"""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = models.UseHistory.objects.all()
    serializer_class = serializers.UseHistorySerializer

    def get_queryset(self):
        """"""

        return self.queryset

    def perform_create(self, serializer):
        """"""

        serializer.save()
