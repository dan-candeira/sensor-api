from djongo import models as dj_models
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,
                                        BaseUserManager,
                                        PermissionsMixin)


class Patient(models.Model):
    """Patients information"""
    cpf = models.CharField(max_length=11, primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_date = models.DateField()
    rg = models.CharField(max_length=7, null=False, unique=True)

    def __str__(self):
        return self.cpf


class SensorType(models.Model):
    """Sensor type"""
    name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.name


class Sensor(models.Model):
    """Sensor informations"""
    type = dj_models.EmbeddedModelField(
        model_container=SensorType
    )
    objects = dj_models.DjongoManager()
    model = models.CharField(max_length=255)

    def __str__(self):
        return self.model


class Equipment(models.Model):
    """Equipment informations"""
    sensors = dj_models.EmbeddedModelField(
        model_container=Sensor
    )
    objects = dj_models.DjongoManager()
    sample_freq = models.IntegerField()
    dimensions = models.CharField(max_length=255)
    description = models.TextField(max_length=450)
    mac_addrs = models.CharField(max_length=17, unique=True, primary_key=True)
    is_available = models.BooleanField(default=False)

    def __str__(self):
        return self.mac_addrs


class UseHistory(models.Model):
    """History of use of the equipment by the patient"""
    loan_date = models.DateTimeField()
    return_date = models.DateField()
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)


class Collect(models.Model):
    """All the data collected from a patient in a given period of time"""
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    start_time = models.TimeField()
    end_time = models.TimeField()
    date = models.DateTimeField()


class Sample(models.Model):
    """Data collected from the sensor"""
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    collect = models.ForeignKey(Collect, on_delete=models.CASCADE)
    date = models.DateTimeField()
    time = models.TimeField()
    capt_data = dj_models.ListField(default=list)
